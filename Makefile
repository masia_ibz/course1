#******************************************************************************
# Copyright (C) 2017 by Alex Fosdick - University of Colorado
#
# Redistribution, modification or use of this software in source or binary
# forms is permitted as long as the files maintain this copyright. Users are 
# permitted to modify this and use it to learn about the field of embedded
# software. Alex Fosdick and the University of Colorado are not liable for any
# misuse of this material. 
#
#*****************************************************************************

#------------------------------------------------------------------------------
# <Put a Description Here>
#
# Use: make [TARGET] [PLATFORM-OVERRIDES]
#
# Build Targets:
#      <Put a description of the supported targets here>
#
# Platform Overrides:
#      <Put a description of the supported Overrides here
#
#------------------------------------------------------------------------------
include sources.mk


# Debug flags
ifeq ($(DEBUG),1)
  	DBGFLAGS = -DCOURSE1
endif
ifeq ($(VERBOSE),1)
 	VBFLAGS = -DVERBOSE
endif

# Platform Overrides

ifeq ($(PLATFORM),HOST) #Obtain data from PLATFORM flag in the command line
	CC = gcc # When HOST, apply GCC native.
	PLATFORM_FLAGS = 
	CPPFLAGS = -DHOST #Preprocessor flags
	LDFLAGS =
	#Instruction to create an OBJS variable as a list of object files based on the sources .c
	SOURCES=$(SOURCES_HOST)
	OBJS = $(SOURCES_HOST:.c=.o)

	#For cleaning
	OBJS_clean_i = $(SOURCES_HOST:.c=.i)
	OBJS_clean_asm = $(SOURCES_HOST:.c=.i)
	OBJS_clean_dep = $(SOURCES_HOST:.c=.c.dep)


	DUMP = objdump

	#GNU BinUtils size
	SIZE=size
else
	CC = arm-none-eabi-gcc # When MSP432, ARM Cross Compiler to be used
	
	# Architectures Specific Compiler Flags (ARM only)
	LINKER_FILE = ./msp432p401r.lds
	CPU = cortex-m4
	ARCH = armv7e-m 
	SPECS = nosys.specs
	#Compiler Architecture Specific flags
PLATFORM_FLAGS = \
	-mcpu=$(CPU) \
	-mthumb \
	-march=$(ARCH) \
	-mfloat-abi=hard \
	-mfpu=fpv4-sp-d16 \
	--specs=$(SPECS) 

	
	CPPFLAGS = -DMSP432 #Preprocessor flags

	#Linker
	LD = arm-none-eabi-ld #ARM linker GNU Binutils. Not clear to what should be used.
	LDFLAGS = -Wl,-Map=$(TARGET).map -T $(LINKER_FILE)

	#Instruction to create an OBJS variable as a list of object files based on the sources .c
	SOURCES=$(SOURCES_ARM)	
	OBJS = $(SOURCES_ARM:.c=.o)


	#For cleaning
	OBJS_clean_i = $(SOURCES_ARM:.c=.i)
	OBJS_clean_asm = $(SOURCES_ARM:.c=.asm)
	OBJS_clean_dep = $(SOURCES_ARM:.c=.c.dep)

	DUMP = arm-none-eabi-objdump

	#GNU BinUtils size
	SIZE=arm-none-eabi-size
	
endif



#General Compiler Flags
CFLAGS = $(INCLUDES) \
	-Wall \
	-Werror \
	-g \
	-O0 \
	-std=c99 

CPP_GENERIC_FLAGS = -DCOURSE1 #Preprocessor General flags

COMPILER_FLAGS=$(CFLAGS) $(PLATFORM_FLAGS) $(CPPFLAGS) $(CPP_GENERIC_FLAGS) $(LDFLAGS)


#String name for the target.out file
TARGET = course1


#To generate an objet file for each source .c file. **Command line: compiler -c Prerequisite_file <GENERALFLAGS PLATFORMFLAGS PREPROCESORFLAGS LINKERFLAGS>-o target_file -D<preprocesormacro> 
%.o : %.c
	$(CC) -MM $< $(COMPILER_FLAGS) > $<.dep
	$(CC) -c $< $(COMPILER_FLAGS)  -o $@ 

$(TARGET).out: $(OBJS) 	
	$(CC) $(OBJS) $(COMPILER_FLAGS)  $(VBFLAGS) $(DBGFLAGS) -lm -o $@
	$(SIZE) -Atd $@

#Generate the preprocessed output of all c-program implementation files (use the –E flag).
%.i : %.c
	$(CC) -E $< $(CPPFLAGS) -o $@


%.asm :  %.o
	$(DUMP) -S --disassemble $< > $@

#Compile-all but do not link


#-PHONY: build-debug
#build-debug: $(OBJS)
#	$(CC) $(OBJS) $(COMPILER_FLAGS)  -lm -o $(TARGET).out 
	
-PHONY: compile-all
compile-all: 
	$(CC) $(SOURCES) $(COMPILER_FLAGS) $(VBFLAGS) $(DBGFLAGS) -lm -c

.PHONY: build
build: $(TARGET).out


#Revisar clean a ver si borra todo.
.PHONY: clean
clean:
	rm -f $(OBJS) $(TARGET).out $(TARGET).map $(OBJS_clean_c) $(OBJS_clean_asm) $(OBJS_clean_dep)




/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * stats.h
 * 
 * Description:
 * File contains all the function declarations used to perform stats.c functionality. (Stadistics report)
 *
 * Author: Carlos Masia
 * Date: 07/09/2019
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */ 

/**
 * Function: find_maximun
 * Description:
 * Finds the maximun of an given array of numbers
 * Note: The max. value of each number would be 255.
 *
 * Parameters:
 * 	unsigned char array: Array of numbers
 * 	unsigned char size: The size of the input array
 *
 * Return:
 * 	Deliver the maximun value of the input array
 */
unsigned char  find_maximun(unsigned char array[], unsigned char size);

/**
 * Function: find_maximun_cell
 * Description:
 * Finds the maximun of an given array of numbers
 * Note: The max. value of each number would be 255.
 *
 * Parameters:
 * 	unsigned char array: Array of numbers
 * 	unsigned char size: The size of the input array
 *	unsigned char max_component: It is an array with two cells. The first one contains the max. value of the input array and the second cell contains the position of the max. value.
 *
 * Return:
 * 	No return. 
 * 	The output information is delivered in the max_component array. 
 */

void find_maximun_cell(unsigned char array[], unsigned char size, unsigned char max_component[]);


/**
 * Function: find_minimun
 * Description:
 * Finds the minimun of an given array of numbers
 * Note: The min. value of each number would be 0 (unsigned variables).
 *
 * Parameters:
 * 	unsigned char array: Array of numbers
 * 	unsigned char size: The size of the input array
 *
 * Return:
 * 	Deliver the minimun value of the input array
 */

unsigned char  find_minimun(unsigned char array[], unsigned char size);


/**
 * Function: find_mean
 * Description:
 * Finds the mean value of an given array of numbers
 * Note: Mean value is calculated by the sum of all the array devidide by the size of the array.
 *
 * Parameters:
 * 	unsigned char array: Array of numbers
 * 	unsigned char size: The size of the input array
 *
 * Return:
 * 	Deliver the mean value of the input array
 */

unsigned char find_mean(unsigned char array[], unsigned char size);

/**
 * Function: find_median
 * Description:
 * Finds the median value of an given array of numbers
 * Note1: Median of an array is the value positioned in the middle of the input array.
 * Note2: If the size number is even the formula is median_cell=((size/2)+(size/2+1))/2. In case of size number is odd, median_cell=(size+1)/2.
 *
 * Parameters:
 * 	unsigned char array: Array of numbers
 * 	unsigned char size: The size of the input array
 *
 * Return:
 * 	Deliver the median value of the input array
 */

unsigned char find_median(unsigned char array[], unsigned char size);

/**
 * Function: sort_array
 * Description:
 * Given an array of data and a length, sorts the array from largest to smallest. 
 * Note1: The zeroth Element should be the largest value, and the last element (n-1) should be the smallest value.
 * 
 *
 * Parameters:
 * 	unsigned char array: Array of numbers
 * 	unsigned char size: The size of the input array
 * 	unsigned char sort_array: This is the sorted array of the input. From largest to smallest)
 *
 * Return:
 * 	No return
 * 	The output information can be found in the sort_array.
 */

void sort_array(unsigned char array[], unsigned char size, unsigned char sort_array[]);

/**
 * Function: print_array
 * Description:
 * Given an array of data and a length, this function prints to the main terminal in a nicely formatted way.
 * 
 * 
 *
 * Parameters:
 * 	unsigned char array: Array of numbers
 * 	unsigned char size: The size of the input array
 * 	
 *
 * Return:
 * 	No return
 * 	
 */

void print_array (unsigned char array[], unsigned char size);

/**
 * Function: print_stadistics
 * Description:
 * Prints outs in a nicely formatted way the stadistics of an input array. 
 * The stadistics of an input array are: Max value, Min value, mean value and median value.
 * 
 *
 * Parameters:
 * 	unsigned char array: Array of numbers
 * 	unsigned char size: The size of the input array
 * 	
 *
 * Return:
 * 	No return
 * 	
 */

void print_stadistics (unsigned char array[], unsigned char size);

#endif /* __STATS_H__ */

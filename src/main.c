/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file main.c
 * @brief Main entry point to the C1M2 Assessment
 *
 * This file contains the main code for the C1M2 assesment. Students
 * are not to change any of the code, they are instead supposed to compile
 * these files with their makefile.
 *
 * @author Alex Fosdick
 * @date April 1 2017
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include "platform.h"
#include "memory.h"
#include "stats.h"
#include "data.h"
#include "course1.h"

typedef unsigned char uint8_t;


#define MAX_LENGTH (10)
char buffer[MAX_LENGTH];

/* A pretty boring main file */
int main(void) {

	#ifdef COURSE1
		course1();

	#else
		unsigned char array_values[2]={1,2};
		uint8_t * ptr;
		uint8_t * dst;

		ptr = (uint8_t *)malloc(2*sizeof(uint8_t)); 
		dst = (uint8_t *)malloc(2*sizeof(uint8_t));
		*ptr=0x56;
		*(ptr+1)=0x48;
		/* Code below does some arbitrary memory Reads & writes */
		print_array(array_values,2);
		PRINTF("**INICIO**\n");
		PRINTF("Puntero ptr 1er byte: %x.\n", *ptr);
		PRINTF("Puntero dst 1er byte: %x\n", *dst);
		PRINTF("**MOVIMIENTO**\n");
		dst=my_memcopy(ptr, dst, 2);
		PRINTF("Puntero ptr 1er byte: %x.\n", *ptr);
		PRINTF("Puntero dst 1er byte: %x\n", *(dst));
		my_itoa(1234, ptr , 10);
		PRINTF("Puntero ptr 1er byte: %c.\n", *ptr);
		PRINTF("Puntero ptr 1er byte: %c.\n", *(ptr+1));
		PRINTF("Puntero ptr 1er byte: %c.\n", *(ptr+2));
		PRINTF("Puntero ptr 1er byte: %c.\n", *(ptr+3));

	#endif




	return 0;
}


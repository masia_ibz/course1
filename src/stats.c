/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * stats.c 
 * 
 * Description:
 * report analytics on the maximum, minimum, mean, and median of the data set. In addition, this program will reorder the input data set from large to small. 
 *
 * @author Carlos Masia
 * @date 06/09/2019
 *
 */



#include <stdio.h>
#include <math.h>
#include "stats.h"
#include "platform.h"

/* Size of the Data Set */
#define SIZE (40)

/*
 void main() {

	unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
		                      114, 88,   45,  76, 123,  87,  25,  23,
		                      200, 122, 150, 90,   92,  87, 177, 244,
		                      201,   6,  12,  60,   8,   2,   5,  67,
		                        7,  87, 250, 230,  99,   3, 100,  90};
	unsigned char array[SIZE];


	printf("*********STADISTICS SOFTWARE*********\n\n");
	printf("The following test array is going to be analyzed. \n");
	print_array(test,SIZE);

	print_stadistics(test,SIZE);

	printf("\nThe given test array is sorted from largest to smallest: \n");
	sort_array(test, SIZE, array);
	print_array(array,SIZE);

} */

/* Add other Implementation File Code Here */
unsigned char find_maximun(unsigned char array[], unsigned char size){
	unsigned char i;
	unsigned char max=0;
	//Error check of empty input array.
	if (array==NULL){
		return 0;
	}

	for (i=0;i<size;i++){
		if(array[i]>max){
			max=array[i];
		}
	}
	return max;
}

void find_maximun_cell(unsigned char array[], unsigned char size, unsigned char max_component[]){
	unsigned char i;
	unsigned char max=0;
	unsigned char max_cell=0;



	for (i=0;i<size;i++){
		if(array[i]>max){
			max=array[i];
			max_cell=i;
		}
	}
	max_component[0]= max;
	max_component[1]= max_cell;
}

unsigned char find_minimun(unsigned char array[], unsigned char size){
	unsigned char i;
	unsigned char min=255; //The biggest value of a unsigned char is 255.
	//Error check of empty input array.
	if (array==NULL){
		return 0;
	}

	for (i=0;i<size;i++){
		if(array[i]<min){
			min=array[i];
		}
	}
	return min;
}
	
unsigned char find_mean(unsigned char array[], unsigned char size){
	unsigned char i;
	unsigned int sum=0; //it will be use to sum all the array's cells.
	unsigned char mean=0;
	//Error check of empty input array.
	if (array==NULL){
		return 0;
	}
	for (i=0;i<size;i++){
		sum=sum+array[i];
	}
	mean=round((float)sum/size); //Round function expects float parameter. Therefore, casting is applied to sum/size. 
	
	return mean;
}


unsigned char find_median(unsigned char array[], unsigned char size){
	unsigned char median_cell=0;
	
	//Error check of empty input array.
	if (array==NULL){
		return 0;
	}

	if(size%2 == 0){ //even size
		median_cell=((size/2)+(size/2+1))/2;
	}
	else{ //odd size
		median_cell=(size+1)/2;
	}
	
	return array[median_cell-1];
}


void sort_array(unsigned char array[], unsigned char size, unsigned char sort_array[]){
	unsigned char i;
	unsigned char max_component [2]= {0,0}; //Array defined as {max_value,max_cell}


	for(i=0;i<size;i++){
		find_maximun_cell(array,size,max_component);	//1st: To extract the max. value and its position on the array (cell)
		sort_array[i]=max_component[0]; //2nd: Save the max. value in the new sort_array.
		array[max_component[1]]=0; //3rd: Eliminate the max. value in the input array.
	}
					
}

void print_array (unsigned char array[], unsigned char size){
	//#ifdef(VERBOSE) 
		unsigned char i;
		printf("{"); 
		for(i=0;i<size;i++){
			printf("%d ", array[i]);
			if (i%10==0 && i!=0){ //Formatting
				PRINTF("\n"); 
			}
		}

		printf("}\n"); 
	//#endif
	
}
		

void print_stadistics (unsigned char array[], unsigned char size){
	printf("\nSummary: \n");
	printf("-->Max value is %d.\n", find_maximun(array,size));
	printf("-->Min value is %d.\n", find_minimun(array,size));
	printf("-->Mean value is %d.\n", find_mean(array,size));
	printf("-->Median value is %d.\n", find_median(array,size));
}



		
		

	

